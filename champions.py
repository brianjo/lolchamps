champion_data = [
    {
        "name": "Ahri",
        "role": "Mid Lane",
        "origin": "Vastaya",
    },
    {
        "name": "Teemo",
        "role": "Top Lane",
        "origin": "Bandle City",
    },
    {
        "name": "Gangplank",
        "role": "Top Lane",
        "origin": "Bilgewater",
    },
    {
        "name": "Sona",
        "role": "Support",
        "origin": "Ionia",
    },
    {
        "name": "Miss Fortune",
        "role": "Marksman",
        "origin": "Bilgewater",
    },
]


def find_champion(name=None, role=None, origin=None):
    """
    Return a list of champions that match the given criteria.

    Parameters
    ----------
    name : str, optional
        The name of the champion to find. (Call this function with no arguments for a 
        full list.)
    role : str, optional
        The role of the champion to find. Roles can include "Mid Lane", "Top Lane",
        "Support", or "Marksman"
    origin : str, optional
        The origin of the champion to find. Origin can include: "Bandle City", "Bilgewater",
        "Ionia", or "Vastaya". 

    Returns
    -------
    list
        A list of champions that match the given criteria.

    Raises
    ------
    ValueError
        If no champions match the given criteria.
    """
    champion_suggestions = []
    for champ in champion_data:
        if name and champ["name"] == name:
            return [champ]
        if role and champ["role"] != role:
            continue
        if origin and champ["origin"] != origin:
            continue
        champion_suggestions.append(champ)
    return champion_suggestions
