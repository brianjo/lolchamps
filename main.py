# This program tests the find_champion function in
# champions.py. 

import champions

print("Calling all champions:")

for champion in champions.find_champion():
    print("{name} ({role}) ({origin})".format(name=champion["name"], role=champion["role"], origin=champion["origin"]))
