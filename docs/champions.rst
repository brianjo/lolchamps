champions module
================

Finding Champion Information
-----------------------------
The `find_champion` function allows you to search for a champion based
on `name`, `role`, or `origin`. You call the function by importing the 
champions module and then calling `find_champion`. 

For example: ::

    import champions
    find_champion(role="Top Lane")

The function produces a list of champions that match your find critera. In 
this case the function would return our two Top Lane heroes, Teemo and Gangplank: ::

    [{'name': 'Teemo', 'role': 'Top Lane', 'origin': 'Bandle City'}, {'name': 'Gangplank', 'role': 'Top Lane', 'origin': 'Bilgewater'}]


You can return a list of all heroes by calling the function with no arguments. 

.. autofunction::  champions.find_champion

