.. LOL Champs documentation master file, created by
   sphinx-quickstart on Mon May 15 18:55:23 2023.

Welcome to LOL Champs!
======================

.. image:: images/champions.png
  :width: 600
  :alt: Champions image

LOL Champs is a Python library that provides fast, programatic access
to information about champions in League of Legends. 

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   champions


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
