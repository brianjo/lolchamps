# LOL Champs
The LOL Champs library provides programmatic access to information about champions in League of Legends. The library features a number of functions for getting detailed information that can be used in a variety of applications.

The file modules.py contains the source for the API. The file main.py provides an example of calling the function(s) in modules.py.

The docs folder contains Sphinx documentation for the project. The output from this project is available as a static HTML site in the `\public` folder. 

To build the documentation:

```
cd docs
pip install -r requirements.txt
make html 
```

You will find the html output in `_build/html`. 

To update the documentation for this project, modify the .rst files in `/docs` and update the documentation strings in the source files and rebuild. 

